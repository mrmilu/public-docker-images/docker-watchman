# BUILD_CMD = docker buildx build --platform linux/arm/v7,linux/arm64/v8,linux/amd64 --pull
BUILD_CMD = docker buildx build --platform linux/arm64/v8,linux/amd64 --pull --push
CI_REGISTRY ?= registry.gitlab.com
CI_REGISTRY_IMAGE ?= $(CI_REGISTRY)/mrmilu/public-docker-images/docker-watchman
# CI_JOB_TOKEN ?= XXXXXXX
CI_REGISTRY_USER ?= UUUUUU
CI_REGISTRY_PASSWORD ?= PPPPPPPPP

build: prepare
	$(BUILD_CMD) --tag $(CI_REGISTRY_IMAGE):4.9.0-alpine3.9 alpine/3.9
	$(BUILD_CMD) --tag $(CI_REGISTRY_IMAGE):4.9.0-buster debian/buster
	$(BUILD_CMD) --tag $(CI_REGISTRY_IMAGE):4.9.0-bionic --tag $(CI_REGISTRY_IMAGE):4.9.0 --tag $(CI_REGISTRY_IMAGE):latest ubuntu/bionic

prepare:
	docker run --privileged --rm tonistiigi/binfmt --install all
	docker buildx create --use --name build-docker-watchman --node build-docker-watchman0
	docker buildx ls

push: prepare
	# docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
	docker login -u $(CI_REGISTRY_USER) -p "$(CI_REGISTRY_PASSWORD)" $(CI_REGISTRY)
	docker push -a $(CI_REGISTRY_IMAGE)